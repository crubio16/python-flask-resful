# -*- coding: utf-8 -*-

from flask import Flask, request, json, Response, jsonify
from flask_restful import Resource, Api
from contextlib import closing
import requests
import json
import csv
from db import UsersDB

app = Flask(__name__)
api = Api(app)


class GetData(Resource):
    def get(self):
        users = []
        user = UsersDB()
        usersdata = user.getusers()
        for r in usersdata:
            row = {"id": r[0], "name": r[1], "email": r[2]}
            users.append(row)

        return Response(
            response=json.dumps(users),
            status=201,
            mimetype="application/json"
        )


class SaveData(Resource):
    def post(self):
        data = request.get_json(force=True)
        url = data['file']

        with closing(requests.get(url, stream=True)) as r:
            f = (line.decode('utf-8') for line in r.iter_lines())
            reader = csv.reader(f, delimiter=',', quotechar='"')
            for row in reader:
                user = UsersDB()
                user.adduser(row[0], row[1])
                #print(row)

            return Response(
                response="Records Added",
                status=201,
                mimetype="application/json"
            )



api.add_resource(GetData, '/')
api.add_resource(SaveData, '/save')

if __name__ == '__main__':
    app.run(debug=True)
